$('.section-countries-carousel').slick({
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
});


$('.section-reviews-carousel').slick({
  centerMode: true,
  // cssEase: 'cubic-bezier(0.950, 0.050, 0.795, 0.035)',
  easing: 'easeOutElastic',
  focusOnSelect: true,
  centerPadding: '0px',
  slidesToShow: 3,
  responsive: [{
    breakpoint: 769,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      slidesToShow: 3
    }
  }, {
    breakpoint: 769,
    settings: {
      arrows: false,
      centerMode: true,
      centerPadding: '40px',
      slidesToShow: 1
    }
  },{
    breakpoint: 768,
    settings: {
      slidesToShow: 1
    }
  }]
});


$('.certificates-carousel').slick({
  slidesToShow: 4,
  infinite: false,
  responsive: [{
    breakpoint: 1000,
    settings: {
      slidesToShow: 4
    }
  }, {
    breakpoint: 650,
    settings: {
      slidesToShow: 3
    }
  },
  {
    breakpoint: 426,
    settings: {
      slidesToShow: 2
    }
  }]
});


$('.partners-carousel').slick({
  slidesToShow: 1,
  infinite: false
});

$('.university-gallery').slick({
  slidesToShow: 1,
  infinite: false
});


$('.team-carousel').slick({
  slidesToShow: 3,
  responsive: [{
    breakpoint: 1000,
    settings: {
      slidesToShow: 2
    }
  }, {
    breakpoint: 650,
    settings: {
      slidesToShow: 1
    }
  }]
});


$('.certificates-carousel').each(function() { // the containers for all your galleries
  $(this).magnificPopup({
      delegate: 'a', // the selector for gallery item
      type: 'image',
      gallery: {
        enabled:true
      }
  });
});


$('.university-gallery').each(function() { // the containers for all your galleries
  $(this).magnificPopup({
      delegate: 'a', // the selector for gallery item
      type: 'image',
      gallery: {
        enabled:true
      }
  });
});

$('.article-gallery-js').each(function() { // the containers for all your galleries
  $(this).magnificPopup({
      delegate: 'a', // the selector for gallery item
      type: 'image',
      gallery: {
        enabled:true
      }
  });
});

let reviewsItems = $(".reviews-slider-item-wrap");
    if(reviewsItems.length < 4) {
      const containerReviews = document.querySelector(".container-reviews");
        if (containerReviews) {
          containerReviews.classList.add("container-mini-reviews");
        }
        $('.reviews-slider-init').slick({
            slidesToShow: 1,
            focusOnSelect: true,
            draggable: false,
            cssEase: 'cubic-bezier(.76,.02,.3,.96)',
            speed: 1200,
            pauseOnHover: false,
            infinite: true,
            pauseOnFocus: false,
            prevArrow: ".reviews-slider-nav-left",
            nextArrow: ".reviews-slider-nav-right",
            centerMode: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    } else {
        $('.reviews-slider-init').slick({
            slidesToShow: 3,
            focusOnSelect: true,
            draggable: false,
            cssEase: 'cubic-bezier(.76,.02,.3,.96)',
            speed: 1200,
            pauseOnHover: false,
            infinite: true,
            pauseOnFocus: false,
            prevArrow: ".reviews-slider-nav-left",
            nextArrow: ".reviews-slider-nav-right",
            centerMode: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });
    }
