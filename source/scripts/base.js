let classToogleBody = (button, addClass) => {

    let body = document.querySelector('body'),
        buttons = document.querySelectorAll(button);

    for(let i = 0; i < buttons.length; i++){
        let btn = buttons[i];
        btn.onclick = () =>{
            body.classList.toggle(addClass);
        }
    }
}

// todo check it

$(".btn-search-js").click(function(){
    $(".search").slideToggle(500);
});

$(".input-phone-mask").mask("+3 80(99) 999-9999");

classToogleBody('.btm-memu-js', "nemu-body-open");


const tableSort = (typeSort) => {
  let tableFilter = document.querySelector(".compare-table-filter tbody");
  let tableFilterItems = document.querySelectorAll(".table-filter-item");
  let sorted = [...tableFilterItems].sort((a,b) => {
    if (typeSort == "up") {
      return - parseInt(a.childNodes[3].getAttribute('data-price')) + parseInt(b.childNodes[3].getAttribute('data-price'))
    }
    else if (typeSort == "down") {
      return parseInt(a.childNodes[3].getAttribute('data-price')) - parseInt(b.childNodes[3].getAttribute('data-price'))
    }

  })
  console.log(sorted);
  tableFilter.innerHTML = ''
  for (let tr of sorted) {
    tableFilter.appendChild(tr);
  }
}

const buttonSort = document.querySelector(".table-sort");
if (buttonSort){
  buttonSort.addEventListener("click", () => {
    if (buttonSort.classList.contains("table-sort-up")){
      tableSort('up');
      buttonSort.classList.add("table-sort-down");
      buttonSort.classList.remove("table-sort-up");
    }
    else if (buttonSort.classList.contains("table-sort-down")){
      tableSort('down');
      buttonSort.classList.add("table-sort-up");
      buttonSort.classList.remove("table-sort-down");
    }
    else{

    }
  })
}

let actionMenu = document.querySelector('.header-actions'),
    bodyHeight = document.querySelector('body');

asideMenuHeight = actionMenu.clientHeight;
windowHeight = window.screen.height;
windowWidth = bodyHeight.clientWidth;
asideMenuPosition = actionMenu.offsetTop;

document.onscroll = function(){
    let windowTop = window.pageYOffset + 70
    if(windowTop >= asideMenuPosition){
        actionMenu.classList.add('header-actions-fixed');
    }
    if(windowTop < asideMenuPosition){
        actionMenu.classList.remove('header-actions-fixed');
    }
}


$(function() {
    $('.select-wrap select').selectric();
});