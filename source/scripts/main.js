// Include you JavaScript Librarys. The following is an example import
////= library/youpathonjsfile.js
//= ../libs/jquery-3.4.1.min.js
//= ../libs/slick/slick.min.js
//= ../libs/selectric/jquery.selectric.js
//= ../scripts/mask.js
//= ../libs/magnific-popup/jquery.magnific-popup.min.js
//= ../scripts/carousel.js
//= ../scripts/tabs.js
//= ../scripts/filter.js
//= ../scripts/moment.js
//= ../scripts/moment-timezone-with-data.js
//= ../scripts/timer.js
//= ../scripts/base.js
//= ../scripts/popups.js
// You JavaScript Code
