tabsMenu('.tabs-nav-item', '.tabs-content-item');
tabsMenu('.history-nav__item', '.history-content__images');
tabsMenu('.advantage-tags-nav__item', '.advantage-tags-content__item');


function tabsMenu(menuItem, contentItem){
  let menuItems = document.querySelectorAll(menuItem),
    contentItems = document.querySelectorAll(contentItem);
  for(let i = 0; i < menuItems.length; i++){
    let menuItem = menuItems[i],
      contentItem = contentItems[i];
    menuItem.onclick = function(){
      for(let j = 0; j < menuItems.length; j++){
        menuItems[j].classList.remove('active');
        contentItems[j].classList.remove('active');
      }
      menuItem.classList.add('active');
      contentItem.classList.add('active');
    }
  }  
}