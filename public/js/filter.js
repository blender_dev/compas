
filterMoreBtn('.filters-item__btn', '.filters-wrapper', 'filters-wrapper-open');
filterMoreBtn('.specialty-more', '.specialty-list-container', 'specialty-list-open');

function filterMoreBtn(buttons, filter, addcalss){
    let btns = document.querySelectorAll(buttons),
        filters = document.querySelectorAll(filter);

    for(let i = 0; i < btns.length; i++){
        let btn = btns[i],
            f = filters[i];
            textBtnDefault = btn.innerHTML;
            textBtn = btn.getAttribute('data-lang');
            btn.onclick = function(){
                f.classList.toggle(addcalss);
                if(f.classList.contains(addcalss)){
                    btn.innerHTML = textBtn;
                }else{
                    btn.innerHTML = textBtnDefault;
                }
            }
    }

}

const openFilterMobile = (btn) => {
  const body = document.querySelector('body')
  const button = $(btn);

  $(button).on('click', () => {
    body.classList.toggle('body-filter-open');
  })
}
openFilterMobile('.btn-open-mobile-filter');
openFilterMobile('.universities-content-bg')