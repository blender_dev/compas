 function openFeedback(){
    $('.btn-feedback').magnificPopup({
		items: [
		  {
			src: '#popup-feedback',
			type: 'inline',
			
		  },
		],
		mainClass: 'mfp-with-zoom', 
	});
}
function openSuccess(){
    $.magnificPopup.open({
        items: {
            src: '#popup-success'
        },
        type: 'inline'
    });
}
function openFavorites(){
	document.querySelector("#popup-favorites").classList.add('popup-action-open');
	setTimeout(function () {
		closeActionPopup()
	}, 2000)
}
function openComparisons(){
	document.querySelector("#popup-comparisons").classList.add('popup-action-open');
	setTimeout(function () {
		closeActionPopup()
	}, 2000)
}
function closeActionPopup() {
	let block = document.querySelectorAll(".popup-action");
	for (let i = 0; i < block.length; i++){
		block[i].classList.remove('popup-action-open');
	}
}
 $(".popup-action-close").on("click", function(){
	 closeActionPopup()
 })
// Успешная отправка формы
// openSuccess();

openFeedback();